const closeTask = () => {
  const close = document.getElementsByClassName("close");

  for (let i = 0; i < close.length; i++) {
    close[i].onclick = function () {
      const task = this.parentElement;
      task.style.display = "none";
    };
  }
};

const tasksList = document.getElementsByTagName("LI");

for (let i = 0; i < tasksList.length; i++) {
  const closeBtn = document.createElement("SPAN");
  const closeSymbol = document.createTextNode("\u00D7");
  closeBtn.className = "close";
  closeBtn.appendChild(closeSymbol);
  tasksList[i].appendChild(closeBtn);
}

closeTask();

const list = document.querySelector("ul");
list.addEventListener(
  "click",
  function (ev) {
    if (ev.target.tagName === "LI") {
      ev.target.classList.toggle("checked");
    }
  },
  false
);

const newTask = () => {
  const task = document.createElement("li");
  const taskText = document.getElementById("newTaskText").value;
  const text = document.createTextNode(taskText);
  task.appendChild(text);

  if (taskText === "") {
    alert("You must write something!");
  } else {
    document.getElementById("tasksList").appendChild(task);
  }

  document.getElementById("newTaskText").value = "";

  const closeBtn = document.createElement("SPAN");
  const closeSymbol = document.createTextNode("\u00D7");
  closeBtn.className = "close";
  closeBtn.appendChild(closeSymbol);
  task.appendChild(closeBtn);

  closeTask();
};
